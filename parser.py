import os
import re
import time
from tqdm import tqdm


def search_files(direct):
    list_files = []
    if os.path.isdir(direct):
        files = os.listdir(direct)
        for i in files:
            root_ext = os.path.splitext(i)
            if root_ext[1] in ['.txt', '.log', '.csv', 'html', '.json']:
                list_files.append(i)
        is_it_folder = True
    elif os.path.isfile(direct):
        is_it_folder = False
        list_files = [direct]
    return is_it_folder, list_files


def open_file_input_str_in_report(file_report, word, i):
    if is_it_folder:
        parent_direct = os.path.join(directory, i)
    else:
        parent_direct = i
    with open(parent_direct, 'r') as file:
        file_report.write("\n" + parent_direct + "\n\n")
        print("Progress:")
        list_strs = file.readlines()
        with tqdm(total=len(list_strs)) as pbar:
            for j in list_strs:
                if re.search(word, j):
                    file_report.write(j)
                pbar.update(1)
                time.sleep(0.00001)


def main():
    global is_it_folder
    global directory

    print("enter the path to the file or folder")
    directory = input()
    print("enter the word, that you want to find")
    word = input()
    is_it_folder, main_files = search_files(directory)
    with open('report.txt', 'w') as f_report:
        for i in main_files:
            open_file_input_str_in_report(f_report, word, i)


main()
